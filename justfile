run: build preview

preview:
    npm run preview

build:
    npm run build

dev:
    npm run dev
