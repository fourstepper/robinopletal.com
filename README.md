# robinopletal.com

This repository is a SvelteKit project that runs [my website](https://robinopletal.com).

[![status-badge](https://ci.codeberg.org/api/badges/12568/status.svg)](https://ci.codeberg.org/repos/12568)

[![Pagespeed Insights](robinopletal-pagespeed.svg)](https://pagespeed.web.dev/analysis?url=https%3A%2F%2Frobinopletal.com%2F)

## Developing

Install [`just`](https://github.com/casey/just)

```bash
just dev
```

## Building

```bash
just build
```

## Build and preview locally

```bash
just run
```

### Shoutouts!

This site would take way more time if it weren't for these resources:

- Josh Collinsworth's blog post: [Let's learn SvelteKit by building a static Markdown blog from scratch](https://joshcollinsworth.com/blog/build-static-sveltekit-markdown-blog)
- The excellent [Svelte](https://svelte.dev/docs/introduction) and [SvelteKit](https://kit.svelte.dev/docs/introduction) documentation
